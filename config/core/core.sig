(* Core configure to allow domtool to be easily relocated *)
signature CONFIG_CORE = sig

    val sharedRoot : string
    (* Global file system space for domtool daemon to share *)

    val localRoot : string
    (* Local file system space for domtool to generate configuration into *)

    val installPrefix : string
    (* Prefix for domtool bin/share data ala GNU $PREFIX *)

    val cat : string
    val cp : string
    val diff : string
    val rm : string
    val echo : string
    val grep : string
    val sudo : string
    (* Paths to standard UNIX utilities *)

end
