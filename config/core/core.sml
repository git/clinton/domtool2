structure ConfigCore :> CONFIG_CORE = struct

val sharedRoot = "/afs/hcoop.net/common/etc/domtool"
val localRoot = "/var/domtool"
val installPrefix = "/usr/local"

val cat = "/bin/cat"
val cp = "/bin/cp"
val diff = "/usr/bin/diff"
val rm = "/bin/rm"
val echo = "/bin/echo"
val grep = "/bin/grep"
val sudo = "/usr/bin/sudo"

end
