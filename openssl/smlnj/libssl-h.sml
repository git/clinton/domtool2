structure LibsslH = struct
    local
	val global = "/usr/local/lib/openssl_sml.so"
	val locl = "openssl/openssl_sml.so"

        val lh = DynLinkage.open_lib
             { name = if Posix.FileSys.access (global, []) then global else locl, global = true, lazy = true }
	    handle DynLinkage.DynLinkError s => raise Fail s
    in
        fun libh s = let
            val sh = DynLinkage.lib_symbol (lh, s)
        in
            fn () => DynLinkage.addr sh
        end
    end
end
