#!/bin/sh
# -*- sh -*-

# This script should go in /etc/init.d/ on Debian Linux systems
# running Domtool dispatchers.

# This script is NOT lsb compliant by a long shot... need to fix that

### BEGIN INIT INFO
# Provides:             domtool-server
# Required-Start:       $remote_fs $network $time openafs-client nscd
# Required-Stop:        $remote_fs $network openafs-client nscd
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Short-Description:    Domtool Dispatcher
# Description:          Launches the domtool server
### END INIT INFO

SELF=$(cd $(dirname $0); pwd -P)/$(basename $0)
PIDFILE="/var/run/domtool/k5start-server.pid"

. /lib/lsb/init-functions

case $1 in
  start)
	log_daemon_msg "Starting Domtool dispatcher" "domtool-server"
	if sudo -u domtool domtool-admin-sudo ping; then
		echo "...already running."
	else
		PIDDIR=$(dirname "$PIDFILE")
		if test ! -d "$PIDDIR"; then
			mkdir -m 0750 $PIDDIR
			chown domtool:nogroup $PIDDIR
		fi

		start-stop-daemon --start --pidfile $PIDFILE \
			-c domtool:nogroup \
			--exec /usr/bin/k5start -- -U -b \
			-f /etc/keytabs/domtool \
			-K 300 -t -p $PIDFILE \
			/usr/local/bin/domtool-server-logged
		echo "."
	fi
	;;

  stop)
	echo -n "Stopping Domtool dispatcher: domtool-server"
	if sudo -u domtool domtool-admin-sudo shutdown; then
		echo "."
	else
		start-stop-daemon --stop --pidfile $PIDFILE
		echo "."
        fi
	rm -f $PIDFILE
	;;

  restart|reload|force-reload)
	set +e; $SELF stop; set -e
	$SELF start
	;;

  status)
	if sudo -u domtool domtool-admin-sudo ping; then
		echo "Domtool dispatcher is running."
	else
		echo "Domtool dispatcher is stopped."
		exit 3
	fi
  	;;

  *)
	echo "Usage: $SELF start|stop|restart|reload|force-reload|status"
	exit 1
	;;
esac
