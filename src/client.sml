(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Code for receiving and executing configuration files *)

structure Client :> CLIENT = struct

datatype passwd_result =
	 Passwd of string
       | Aborted
       | Error

fun getpass () =
    let
	val tty = Posix.FileSys.stdin
	val termios = SOME (Posix.TTY.TC.getattr tty)
		      handle OS.SysErr (reason, SOME syserr) =>
			     if syserr = Posix.Error.notty then
				 (print "Warning: no terminal found, not hiding password\n";
				  TextIO.flushOut TextIO.stdOut;
				  NONE)
			     else raise OS.SysErr (reason, SOME syserr)
	val fields = case termios of SOME termios => SOME (Posix.TTY.fieldsOf termios)
				   | NONE => NONE

	val termios' = case fields of SOME fields =>
				      SOME (Posix.TTY.termios {iflag = #iflag fields,
							       oflag = #oflag fields,
							       cflag = #cflag fields,
							       lflag = Posix.TTY.L.flags [Posix.TTY.L.clear (Posix.TTY.L.echo, #lflag fields),
											  Posix.TTY.L.echonl,
											  Posix.TTY.L.icanon],
							       cc = #cc fields,
							       ispeed = #ispeed fields,
							       ospeed = #ospeed fields})
				    | NONE => NONE

	fun reset () = case termios of SOME termios => Posix.TTY.TC.setattr (tty, Posix.TTY.TC.sanow, termios)
				     | NONE => ()
    in
	print "        Password: ";
	TextIO.flushOut TextIO.stdOut;
	case termios' of SOME termios' => Posix.TTY.TC.setattr (tty, Posix.TTY.TC.sanow, termios')
		       | NONE => ();
	case TextIO.inputLine TextIO.stdIn of
	    NONE => (reset ();
		     Aborted)
	  | SOME pass =>
	    (print "Confirm password: ";
	     TextIO.flushOut TextIO.stdOut;
	     case TextIO.inputLine TextIO.stdIn of
		 NONE => (reset ();
			  Aborted)
	       | SOME pass' =>
		 (reset ();
		  if pass = pass' then
		      Passwd (String.substring (pass, 0, size pass - 1))
		  else
		      (print "Passwords don't match!\n";
		       Error)))
    end

end
