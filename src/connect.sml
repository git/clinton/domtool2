(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2014 Clinton Ebadi <clinton@unknownlamer.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

structure Connect :> CONNECT = struct

open MsgTypes

fun requestDispatcherBio context =
    OpenSSL.connect true (context, Domain.nodeIp Config.dispatcherName ^ ":" ^ Int.toString Config.dispatcherPort)

fun requestWorkerBio (context, node) =
    OpenSSL.connect true (context, Domain.nodeIp node ^ ":" ^ Int.toString Config.slavePort)

fun commandWorker (context, node, message) = 
(* It might be worth making this context -> node -> message -> bool *)
    let
	val bio = requestWorkerBio (context, node)
    in
	Msg.send (bio, message);
	(case Msg.recv bio of
	     NONE => (print "Slave closed connection unexpectedly\n";
		      false)
	   | SOME m =>
	     case m of
		 MsgOk => (print ("Slave " ^ node ^ " finished\n");
			   true)
	       | MsgError s => (print ("Slave " ^ node
				       ^ " returned error: " ^
				       s ^ "\n");
				false)
	       | _ => (print ("Slave " ^ node
			      ^ " returned unexpected command\n");
		       false))
	before OpenSSL.close bio
	handle OpenSSL.OpenSSL s =>  (print ("OpenSSL error: " ^ s ^ "\n");
				      OpenSSL.close bio;
				      false
				      handle OpenSSL.OpenSSL _ => false)
    end
    handle OpenSSL.OpenSSL s =>  (print ("OpenSSL error: " ^ s ^ "\n");
				  false)

end
