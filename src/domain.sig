(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Domain-related primitive actions *)

signature DOMAIN = sig

    val declareClient : unit -> unit
    val fakePrivileges : unit -> unit

    val yourPath : string -> bool
    val isIdent : char -> bool
    val validHost : string -> bool
    val validDomain : string -> bool
    val yourDomain : string -> bool
    val validUser : string -> bool
    val validEmailUser : string -> bool

    val ip : string Env.arg

    val registerResetGlobal : (unit -> unit) -> unit
    val registerResetLocal : (unit -> unit) -> unit
    (* Register functions for clearing out all Domtool configuration at the global
     * (AFS) and local levels, respectively. *)

    val resetGlobal : unit -> unit
    val resetLocal : unit -> unit
    (* Call all registered functions *)

    val registerBefore : (string -> unit) -> unit
    val registerAfter : (string -> unit) -> unit
    (* Register handlers to run just before and after entering a domain
     * block. *)

    val currentDomain : unit -> string

    val domainFile : {node : string, name : string} -> TextIO.outstream
    (* Open one of the current domain's configuration files for a particular
     * node. *)

    val currentAliasDomains : unit -> string list
    val currentDomains : unit -> string list
    (* Return the auxiliary domains being configured (not including
     * currentDomain) or the list of all domains being configured,
     * respectively. *)

    (* The type of a set of files open for different domains. *)
    type files = {write : string -> unit,  (* Write a string to each. *)
		  writeDom : unit -> unit, (* Write each's domain name to it. *)
		  close : unit -> unit}    (* Close all files. *)

    val domainsFile : {node : string, name : string} -> files
    (* Open a configuration file for every domain being configured. *)

    val dnsMaster : unit -> string option
    (* Name of the node that is the DNS master for the current domain, if there
     * is one *)

    val nodes : string list
    (* Names of all system nodes *)
    val nodeMap : string Ast.StringMap.map
    (* Map node names to IP addresses *)
    val nodeIp : string -> string
    (* Look up a node in nodeMap *)

    val setUser : string -> unit
    val getUser : unit -> string
    (* Name of the UNIX user providing this configuration *)

    val your_domains : unit -> DataStructures.StringSet.set
    (* The domains the current user may configure *)

    val your_users : unit -> DataStructures.StringSet.set
    val your_groups : unit -> DataStructures.StringSet.set
    val your_paths : unit -> DataStructures.StringSet.set
    (* UNIX users, groups, and paths the user may act with *)

    val get_context : unit -> OpenSSL.context
    val set_context : OpenSSL.context -> unit

    val hasPriv : string -> bool

    val rmdom : string list -> unit
    val rmdom' : string -> string list -> unit

    val homedirOf : string -> string
    val homedir : unit -> string

    type subject = {node : string, domain : string}

    val registerDescriber : (subject -> string) -> unit
    (* When a user runs [domtool-admin describe $DOM], every function passed to
     * [registerDescriber] will be run on [$DOM]. *)
    val describe : string -> string
    (* Argument is domain to describe, over all nodes. *)

    datatype description =
	     Filename of { filename : string, heading : string, showEmpty : bool }
	   | Extension of { extension : string, heading : string -> string }

    val considerAll : description list -> subject -> string
    (* Find files in a domain directory matching some patterns and generate
     * headings and contents listings for them. *)

    (* Callbacks to run whenever the set of Domtool users has changed *)
    val registerOnUsersChange : (unit -> unit) -> unit
    val onUsersChange : unit -> unit
end
