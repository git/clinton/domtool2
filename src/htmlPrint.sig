(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Pretty-printing Domtool configuration file ASTs for HTML *)

signature HTML_PRINT = sig

val setProviders : Order.providers -> unit

structure PD : PP_DESC

val p_pred : Ast.pred -> PD.pp_desc
val p_typ : Ast.typ -> PD.pp_desc
val p_exp : Ast.exp -> PD.pp_desc
val p_decl : Ast.decl' -> PD.pp_desc
val p_decl_fref : Ast.decl' -> PD.pp_desc

val output : PD.pp_desc -> HTML.text

end
