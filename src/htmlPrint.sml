(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Pretty-printing Domtool configuration file ASTs for HTML *)

signature HTML_PRINT_ARG = sig
    include PRINTFN_INPUT where type rendering = HTML.text

    val setProviders : Order.providers -> unit
end

structure HtmlPrintArg :> HTML_PRINT_ARG = struct

open Ast Order

structure TextToken = struct
type token = string
type style = HTMLDev.style
fun string t = t
fun style t = HTMLDev.styleTT
fun size t = String.size t
end

structure SM = PPStreamFn(structure Token = TextToken
                          structure Device = HTMLDev)

structure PD = PPDescFn(SM)
open PD

fun keyword s = style (HTMLDev.styleB, [string s])
val punct = string
val field = string
val lit = string
val ident = string

val prov : providers option ref = ref NONE
fun setProviders p = prov := SOME p

fun modify file =
    let
	val file' = #file (OS.Path.splitDirFile file)
	val file' = #base (OS.Path.splitBaseExt file')
    in
	file'
    end

fun context s =
    case providesContext (valOf (!prov), s) of
	NONE => string s
      | SOME m => style (HTMLDev.link (modify m ^ ".html#C_" ^ s), [string s])
fun typ s =
    case providesType (valOf (!prov), s) of
	NONE => string s
      | SOME m => style (HTMLDev.link (modify m ^ ".html#T_" ^ s), [string s])
fun exp s =
    case providesValue (valOf (!prov), s) of
	NONE => string s
      | SOME m => style (HTMLDev.link (modify m ^ ".html#V_" ^ s), [string s])

fun anchor (s, d) = style (HTMLDev.anchor s, [d])
fun link (s, d) = style (HTMLDev.link s, [d])

type rendering = HTML.text
fun openStream () =
    let
	val dev = HTMLDev.openDev {wid = 80,
				   textWid = NONE}
    in
	SM.openStream dev
    end
fun closeStream s = HTMLDev.done (SM.getDevice s)

end

structure HtmlPrint = PrintFn(HtmlPrintArg)
