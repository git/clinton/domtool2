(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2014, Clinton Ebadi <clinton@unknownlamer.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Portal helper utility. *)

(* Duplicated from main-config.sml, should be put into a common module
   and all domtool commands should return proper exit codes instead of
   always succeeding *)

fun println x = (print x; print "\n")
fun printerr x = (TextIO.output (TextIO.stdErr, x); TextIO.flushOut TextIO.stdErr)
fun die reason = (printerr reason; printerr "\n"; OS.Process.exit OS.Process.failure)

val _ =
    case CommandLine.arguments () of
	["vmailpasswd", domain, user] =>
	(case Client.getpass () of
	     Client.Passwd oldpasswd =>
	     (case Client.getpass () of 
		  Client.Passwd newpasswd =>
		  Main.requestPortalPasswdMailbox {domain = domain,
						   user = user,
						   oldpasswd = oldpasswd,
						   newpasswd = newpasswd}
		  | Client.Aborted => die "Aborted"
		  | Client.Error => die "New passwords did not match")
	   | _ => die "Error entering old password")
      | _  => die "Invalid command-line arguments"
	
