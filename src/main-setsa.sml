(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Driver for SpamAssassin filtering preferences *)

val _ =
    let
	fun defaultEmail () = Posix.SysDB.Passwd.name (Posix.SysDB.getpwuid (Posix.ProcEnv.getuid ()))

	val args = CommandLine.arguments ()

	val (addr, args) =
	    case args of
		"on" :: _ => (defaultEmail (), args)
	      | "off" :: _ => (defaultEmail (), args)
	      | addr :: rest => (addr, rest)
	      | [] => (defaultEmail (), args)
    in
	case args of
	    [] => Main.requestSaQuery addr
	  | ["on"] => Main.requestSaSet (addr, true)
	  | ["off"] => Main.requestSaSet (addr, false)
	  | _ => print "Invalid command-line arguments\n"
    end
