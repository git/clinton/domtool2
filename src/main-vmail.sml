(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Driver for vmail *)

val _ =
    case CommandLine.arguments () of
	[] => print "Invalid command-line arguments\n"
      | domain :: rest =>
	case rest of
	    ["list"] =>
	    (case Main.requestListMailboxes domain of
		 Vmail.Error msg => (print msg;
				     print "\n")
	       | Vmail.Listing users => app (fn {user, mailbox} => (print user;
								    print "\t";
								    print mailbox;
								    print "\n")) users)

	  | ["add", user, mailbox] =>
	    (case Client.getpass () of
		 Client.Passwd passwd =>
		 let
		     val mailbox = if size mailbox > 0
				      andalso String.sub (mailbox, 0) <> #"/" then
				       let
					   val uid = Posix.ProcEnv.getuid ()
					   val user = Posix.SysDB.getpwuid uid
					   val home = Posix.SysDB.Passwd.home user
				       in
					   home ^ "/" ^ mailbox
				       end
				   else
				       mailbox
		 in
		     Main.requestNewMailbox {domain = domain,
					     user = user,
					     passwd = passwd,
					     mailbox = mailbox}
		 end
	       | _ => ())

	  | ["passwd", user] =>
	    (case Client.getpass () of
		 Client.Passwd passwd =>
		 Main.requestPasswdMailbox {domain = domain,
					    user = user,
					    passwd = passwd}
	       | _ => ())

	  | ["rm", user] =>
	    (print ("Are you sure you want to delete the mapping for " ^ user ^ "@" ^ domain ^ "? (yes/no) ");
	     if TextIO.inputLine TextIO.stdIn = SOME "yes\n" then
		 Main.requestRmMailbox {domain = domain,
					user = user}
	     else
		 print "Aborted\n")
	    
	  | _ => print "Invalid command-line arguments\n"
