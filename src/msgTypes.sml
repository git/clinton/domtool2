(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 * Copyright (c) 2011,2014 Clinton Ebadi <clinton@unknownlamer.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Network message data structures *)

structure MsgTypes = struct

datatype socket_permission =
	 Any
       | Client
       | Server
       | Nada

datatype query =
	 QApt of string
       (* Is this apt package installed? *)
       | QCron of string
       (* Is this user allowed to use cron? *)
       | QFtp of string
       (* Is this user allowed to use FTP? *)
       | QTrustedPath of string
       (* Is this user restricted to trusted-path executables? *)
       | QSocket of string
       (* What socket permissions does this user have? *)
       | QFirewall of {node : string, user : string}
       (* What firewall rules does this user have? *)
       | QAptExists of string
       (* Does this apt package exist *)

datatype msg =
	 MsgOk
       (* Your request was processed successfully. *)
       | MsgError of string
       (* Your request went wrong in some way. *)
       | MsgConfig of string
       (* Configuration source code *)
       | MsgFile of Slave.file_status
       (* The status of a configuration file has changed. *)
       | MsgDoFiles
       (* Perform the actions associated with the MsgFiles sent previously. *)
       | MsgGrant of Acl.acl
       (* Grant a permission *)
       | MsgRevoke of Acl.acl
       (* Revoke a permission *)
       | MsgListPerms of string
       (* List all of a user's permissions *)
       | MsgPerms of (string * string list) list
       (* A response to MsgListPerms, giving a permission class and all values
	* for which the user is authorized in that class *)
       | MsgWhoHas of {class : string, value : string}
       (* Which users have this permission? *)
       | MsgWhoHasResponse of string list
       (* These are the users! *)
       | MsgMultiConfig of string list
       (* Multiple Domtool sources in dependency order *)
       | MsgRmdom of string list
       (* Remove all configuration associated with some domains and revoke
	* rights to those domains from all users. *)
       | MsgRegenerate
       (* Make a clean slate of it and reprocess all configuration from scratch. *)
       | MsgRmuser of string
       (* Remove all ACL entries for a user, and remove all domains to which
	* that user and no one else has rights. *)
       | MsgCreateDbUser of {dbtype : string, passwd : string option}
       (* Request creation of a user for the named DBMS type *)
       | MsgCreateDb of {dbtype : string, dbname : string, encoding : string option}
       (* Request creation of a DBMS database *)
       | MsgDropDb of {dbtype : string, dbname : string}
       (* Request dropping of a DBMS database *)
       | MsgNewMailbox of {domain : string, user : string,
			   passwd : string, mailbox : string}
       (* Request creation of a new vmail mapping *)
       | MsgPasswdMailbox of {domain : string, user : string, passwd : string}
       (* Change a vmail account's password *)
       | MsgPortalPasswdMailbox of {domain : string, user : string, oldpasswd : string, newpasswd : string}
       (* Change a vmail account's password if the old password matches *)
       | MsgRmMailbox of {domain : string, user : string}
       (* Remove a vmail mapping *)
       | MsgListMailboxes of string
       (* List all mailboxes for a domain *)
       | MsgMailboxes of {user : string, mailbox : string} list
       (* Reply to MsgListMailboxes *)
       | MsgSaQuery of string
       (* Check on the SpamAsssassin filtering status of a user or e-mail address *)
       | MsgSaStatus of bool
       (* Response to MsgSaQuery *)
       | MsgSaSet of string * bool
       (* Set the filtering status of a user or e-mail address *)
       | MsgSaChanged
       (* Reload spamassassin addrs *)
       | MsgSmtpLogReq of string
       (* Request all current SMTP log lines about a domain *)
       | MsgSmtpLogRes of string
       (* One line of a response to MsgSmtpLogReq *)
       | MsgDbPasswd of {dbtype : string, passwd : string}
       (* Change a DBMS user's password *)
       | MsgShutdown
       (* Halt the server *)
       | MsgYes
       | MsgNo
       (* Answers to boolean queries *)
       | MsgQuery of query
       (* Ask for host-specific information *)
       | MsgSocket of socket_permission
       (* Answer to a QSocket query *)
       | MsgFirewall of string list
       (* Answer to a QFirewall query *)
       | MsgRegenerateTc
       (* MsgRegenerate without actual publishing of configuration *)
       | MsgGrantDb of {dbtype : string, dbname : string}
       (* Grant all allowed privileges on a DBMS database to the user *)
       | MsgMysqlFixperms
       (* Run the script to grant DROP privileges on MySQL tables to owning users *)
       | MsgDescribe of string
       (* Ask for a listing of all of a domain's real configuration *)
       | MsgDescription of string
       (* Reply to MsgDescribe *)
       | MsgReUsers
       (* Rerun all callbacks for cases where the set of users has changed *)
       | MsgVmailChanged
       (* Server tells slave that vmail user information has changed *)
       | MsgFirewallRegen
       (* Regenerate firewall on user machines *)
       | MsgAptQuery of {section : string, description : string}
       (* Answer to QAptExists query *)

end
