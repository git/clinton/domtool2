(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* Apache HTTPD handling *)

signature APACHE = sig

    val registerPre : ({user : string, nodes : string list, id : string, hostname : string} -> unit) -> unit
    (* Register a callback for the beginning of a vhost block. *)

    val registerPost : (unit -> unit) -> unit
    (* Register a callback for the end of a vhost block. *)

    val doPre : {user : string, nodes : string list, id : string, hostname : string} -> unit
    val doPost : unit -> unit

    val registerAliaser : (string -> unit) -> unit
    (* Register a callback for an alternate hostname that is configured. *)

    val logDir : {user : string, node : string, vhostId : string} -> string
    (* Where is a vhost's log directory located? *)

    val realLogDir : {user : string, node : string, vhostId : string} -> string
    (* OK, where is it _really_ located?  (Target of log syncing into AFS) *)

    val ssl : string option Env.arg
    val webPlace : (string * string) Env.arg

    val webNode : string -> bool
end
