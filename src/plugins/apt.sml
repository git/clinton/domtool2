(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006-2007, Adam Chlipala
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* APT package database querying *)

structure Apt :> APT = struct

fun validName s = CharVector.all (fn ch => Char.isAlphaNum ch orelse ch = #"_" orelse ch = #"-" orelse ch = #".") s
		  andalso (size s > 0 andalso String.sub (s, 0) <> #"-")

(* Copyed from the portal, doesn't exactly go out this in the most
   direct way, or does it? *)

fun info name =
    if not (validName name) then
	NONE
    else
	let
	    val proc = Unix.execute ("/usr/bin/apt-cache", ["show", name])
	    val inf = Unix.textInstreamOf proc
		      
	    val _ = TextIO.inputLine inf (* in every let* lies an imperative program in disguise *)
		    
	    fun loop (section, descr) =
		case TextIO.inputLine inf of
		    NONE => (section, descr)
		  | SOME line =>
		    if size line >= 9 andalso String.substring (line, 0, 9) = "Section: " then
			loop (SOME (String.substring (line, 9, size line - 10)), descr)
		    else if size line >= 13 andalso String.substring (line, 0, 13) = "Description: " then
			loop (section, SOME (String.substring (line, 13, size line - 14)))
		    else
			loop (section, descr)
	in
	    (case loop (NONE, NONE) of
		 (SOME section, SOME descr) => SOME {section = section, description = descr}
	       | _ => NONE)
	    before ignore (Unix.reap proc)
	end
			   
fun installed name =
    validName name
    andalso let
	val proc = Unix.execute ("/usr/bin/apt-cache", ["policy", name])
        val inf = Unix.textInstreamOf proc

	val _ = TextIO.inputLine inf
    in
	(case TextIO.inputLine inf of
	     NONE => false
	   | SOME line =>
	     case String.tokens Char.isSpace line of
		 [_, "(none)"] => false
	       | [_, _] => true
	       | _ => false)
	before ignore (Unix.reap proc)
    end

end
