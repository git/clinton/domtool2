(* HCoop Domtool (http://hcoop.sourceforge.net/)
 * Copyright (c) 2006, Adam Chlipala
 * Copyright (c) 2012 Clinton Ebadi <clinton@unknownlamer.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *)

(* PostgreSQL user/table management *)

structure Postgres :> POSTGRES = struct

fun adduser port {user, passwd} =
    Option.map (fn s => "Error executing CREATE USER script:\n" ^ s)
	       (Slave.shellOutput [Config.Postgres.adduser, " ", port, " ", user])

fun passwd _ = SOME "We don't use PostgreSQL passwords."

fun createdb port {user, dbname, encoding} =
    Option.map (fn s => "Error executing CREATE DATABASE script:\n" ^ s)
	       (Slave.shellOutput [Config.Postgres.createdb,
				   " ", port, " ", user, " ", dbname,
				   case encoding of NONE => "" | SOME e => " " ^ e])

fun dropdb port {user, dbname} =
    Option.map (fn s => "Error executing DROP DATABASE script:\n" ^ s)
	       (Slave.shellOutput [Config.Postgres.dropdb, " ", port, " ", user, " ", dbname])

val _ = Dbms.register ("postgres", {getpass = NONE,
				    adduser = adduser Config.Postgres.postgres81port,
				    passwd = passwd,
				    createdb = createdb Config.Postgres.postgres81port,
				    dropdb = dropdb Config.Postgres.postgres81port,
				    grant = fn _ => SOME "You don't need to use GRANT for Postgres."})

val _ = Dbms.register ("postgres-9.1", {getpass = NONE,
					adduser = adduser Config.Postgres.postgres91port,
					passwd = passwd,
					createdb = createdb Config.Postgres.postgres91port,
					dropdb = dropdb Config.Postgres.postgres91port,
					grant = fn _ => SOME "You don't need to use GRANT for Postgres."})

end
